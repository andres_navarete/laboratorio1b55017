package cql.ecci.ucr.ac.cr.sodauniversitaria;

import android.os.Parcel;
import android.os.Parcelable;

public class Persona implements Parcelable {

    private String identificacion;
    private String correo;
    private String nombre;
    private String primerApellido;
    private String segundoApellido;
    private String celular;

    public Persona(
        String identificacion,
        String correo,
        String nombre,
        String primerApellido,
        String segundoApellido,
        String celular)
    {
        this.setIdentificacion(identificacion);
        this.setCorreo(correo);
        this.setNombre(nombre);
        this.setPrimerApellido(primerApellido);
        this.setSegundoApellido(segundoApellido);
        this.setCelular(celular);
    }

    public Persona() {
        this.setIdentificacion("B55017");
        this.setCorreo("andres.navarrete@ucr.ac.cr");
        this.setNombre("Andres");
        this.setPrimerApellido("Navarrete");
        this.setSegundoApellido("Boza");
        this.setCelular("85819696");
    }

    protected Persona(Parcel in) {
        setIdentificacion(in.readString());
        setCorreo(in.readString());
        setNombre(in.readString());
        setPrimerApellido(in.readString());
        setSegundoApellido(in.readString());
        setCelular(in.readString());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(getIdentificacion());
        dest.writeString(getCorreo());
        dest.writeString(getNombre());
        dest.writeString(getPrimerApellido());
        dest.writeString(getSegundoApellido());
        dest.writeString(getCelular());
    }
    @Override
    public int describeContents() {
        return 0;
    }
    public static final Creator<Persona> CREATOR = new Creator<Persona>() {
        @Override
        public Persona createFromParcel(Parcel in) {
            return new Persona(in);
        }
        @Override
        public Persona[] newArray(int size) {
            return new Persona[size];
        }
    };

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPrimerApellido() {
        return primerApellido;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    public String getSegundoApellido() {
        return segundoApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    @Override
    public String toString() {
        return nombre + " " + primerApellido + " " + segundoApellido;
    }
}
